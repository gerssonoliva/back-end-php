<?php
    require_once 'vendor/autoload.php';

    $app = new \Slim\Slim();

    //conectado a bd
    $db = new mysqli('localhost','root','','bd_curso_angular');

    $app->get('/pruebas', function() use($app, $db){
        echo 'Hola mundo';
    });

    //CONFIGURACIÓN DE CABECERAS HTTP
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Allow: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }

    //LISTAR TODOS LOS PRODUCTOS
    $app->get('/productos', function() use($app, $db){
        $sql = "SELECT * FROM productos ORDER BY id DESC";
        $query = $db->query($sql);
        //var_dump($query->fetch_all());
        $productos = array();
        while ($producto = $query->fetch_assoc()) {
            $productos[] = $producto;
        }
        $result = array(
            'status' => 'success',
            'code'   => 200,
            'data'   => $productos
        );
        echo json_encode($result);
    });

    //DEVOLVER UN SOLO PRODUCTO
    $app->get('/producto/:id', function($id) use($app, $db){
        $sql = "SELECT * FROM productos WHERE id = ".$id;
        $query = $db->query($sql);
        $result = array(
            'status' => 'error',
            'code'   => 404,
            'data'   => "Producto no disponible"
        );
        if ($query->num_rows==1) {
            $producto = $query->fetch_assoc();
        }
        $result = array(
            'status' => 'success',
            'code'   => 200,
            'data'   => $producto
        );
        echo json_encode($result);
    });

    //ACTUALIZAR UN PRODUCTO
    $app->post('/producto-edit/:id', function($id) use($app, $db){
        //recoger los datos que lleguen de algun formulario
        $json = $app->request->post('json');
        //decodificar el json
        $data = json_decode($json, true);
        $sql = "UPDATE productos SET ".
                "nombre = '{$data["nombre"]}',".
                "descripcion = '{$data["descripcion"]}',";
        if (isset($data['image'])) {
            $sql .= "image = '{$data["image"]}',";
        }
        $sql .= "precio = '{$data["precio"]}' WHERE id = $id"; 
        $query = $db->query($sql);
        if ($query) {
            $result = array(
                'status' => 'success',
                'code'   => 200,
                'data'   => 'El producto ha sido actualizado'
            );
        }else{
            $result = array(
                'status' => 'error',
                'code'   => 404,
                'data'   => "No se pudo actualizar el producto"
            );
        }
        echo json_encode($result);
    });  

    //ELIMINAR UN PRODUCTO
    $app->get('/producto-delete/:id', function($id) use($app, $db){
        $sql = "DELETE FROM productos WHERE id = ".$id;
        $query = $db->query($sql);
        $result = array(
            'status' => 'error',
            'code'   => 404,
            'data'   => "No se pudo eliminar el producto"
        );
        
        $result = array(
            'status' => 'success',
            'code'   => 200,
            'data'   => 'Se eliminó el producto'
        );
        echo json_encode($result);
    });

    //SUBIR UNA IMAGEN A UN PRODUCTO
    $app->post('/upload-file', function() use($app, $db){
        $result = array(
            'status'  => 'error',
            'code'    => 404,
            'message' => 'El archivo no pudo cargarse'
        );
        if (isset($_FILES['uploads'])) {
            $piramideUploader = new PiramideUploader();
            $upload = $piramideUploader->upload('image','uploads', 'uploads', array('image/jpeg','image/png','image/gif'));//(_nombre del fichero, _name del fichero que llega por $FILE, _directorio donde se guardará)
            $file = $piramideUploader->getInfoFile();//Info del fichero subido
            $file_name = $file['complete_name'];
            if (isset($upload) && $upload['uploaded'] == false) {
                $result = array(
                    'status'  => 'error',
                    'code'    => 404,
                    'message' => 'El archivo no pudo cargarse'
                );
            }else{
                $result = array(
                    'status'   => 'success',
                    'code'     => 200,
                    'data'     => 'El archivo se cargó correctamente',
                    'filename' => $file_name
                );
            }
        }
        echo json_encode($result);
    });
    //GUARDAR PRODUCTOS
    $app->post('/productos', function() use($app, $db){
        //recoger los datos que lleguen de algun formulario
        $json = $app->request->post('json');
        //decodificar el json
        $data = json_decode($json, true);
        
        if (!isset($data['nombre'])) {
            $data['nombre'] = null;
        }
        if (!isset($data['descripcion'])) {
            $data['descripcion'] = null;
        }
        if (!isset($data['precio'])) {
            $data['precio'] = null;
        }
        if (!isset($data['imagen'])) {
            $data['imagen'] = null;
        }
        $sql = "INSERT INTO productos VALUES(NULL,".
                "'{$data['nombre']}',".
                "'{$data['descripcion']}',".
                "'{$data['precio']}',".
                "'{$data['imagen']}'".
                ")";
        $insert = $db->query($sql);
        
        $result = array(
            'status'  => 'error',
            'code'    => 404,
            'message' => 'El producto no ha sido creado correctamente'
        );

        if ($insert) {
            $result = array(
                'status'  => 'seccess',
                'code'    => 200,
                'message' => 'Producto creado correctamente'
            );
        }

        echo json_encode($result);

    });

    $app->run();
?>