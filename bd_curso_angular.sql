CREATE DATABASE IF NOT EXISTS bd_curso_angular;
USE bd_curso_angular;

CREATE TABLE productos(
    id          int(10) auto_increment not null,
    nombre      varchar(255),
    descripcion text,
    precio      varchar(10),
    imagen      varchar(255),
    CONSTRAINT pk_productos PRIMARY KEY (id)
)ENGINE=InnoDb;